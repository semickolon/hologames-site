---
title: "Sploot"
---

Found in `games/sploot.py`.

## Gameplay

Sploot is an infinite runner game where the player plays as a corgi named Bread, who must jump over logs and dodge flying frisbees. Each run gives the player three lives.

The game also features a multiplayer option, which allows two players to play the game simultaneously (one as Bread, and one as a Shiba Inu, Lettuce).

Points in the game are earned per second.


## Controls

Each player must use the directional keys (up and down) to either jump over or sploot under obstacles. Failure to dodge will reduce a life.


## Depth Usage

The hologame is separated into two layers: the background and the foreground. The characters and obstacles are placed in the foreground -- this gives the game an interesting take on depth, and makes the characters pop out more. The background scrolls infinitely behind the foreground screen.
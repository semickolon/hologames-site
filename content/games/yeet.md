---
title: "Yeet"
---

Found in `games/yeet.py`.

## Gameplay

Yeet is a multiplayer pong/table tennis sports game. Both players can control their paddle with their controller. The first to reach 11 points is considered the winner.


## Controls

Players can control their paddle with the directional keys (up and down) in order to hit the ball back.


## Depth Usage

The hologame is separated into two layers: the background and the foreground. The paddle and ball are placed in the foreground -- this makes it pop out more and easily makes it the player's point of focus.
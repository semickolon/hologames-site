---
title: "Animator"
---

Found in `component.py`.
Inherits from Behavior.

Allows certain parts of a game to be animated, and streamlines the use of animated assets within the game. Allows each animated object to start, pause and stop its animation, etc.
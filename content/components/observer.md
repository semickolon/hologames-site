---
title: "Observer"
---

Found in `component.py`.

An Observer class listens to events.
---
title: "ObserverSubject"
---

Found in `component.py`.

ObserverSubject can add and remove observers. It may also send instructions to other components depending on the occurence of said events.
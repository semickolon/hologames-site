---
title: "GameObject"
---

Found in `component.py`.

GameObjects are similar to Pyglet sprites, and are essentially potentially moving and controllable images or animations. The key difference between a GameObject and a Pyglet sprite is that GameObjects possess a layer index, which allows it to be positioned in whichever layer/s the developer sees fit.
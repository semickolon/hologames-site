---
title: "Screen"
---

Found in `screen.py`.

The Screen stores GameObjects which it updates and draws accordingly on their corresponding layers. It also acts as the orchestrator of its GameObjects. Managed by its parent ScreenStack instance.
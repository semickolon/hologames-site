---
title: "ScreenStack"
---

Found in `screen.py`.

ScreenStack organizes Screens. It can push a screen by adding it to the list screens, pop a screen by removing it from said list, and select which screen is currently displayed. It is also the parent class of Screen. 
---
title: "Server"
---

Found in `server.py`.

Communicates with client.py and, by extension, the hologames controller. Facilitates communication between player input and game.
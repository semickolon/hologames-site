---
title: "Layer"
---

Found in `layer.py`.

Games made for the Hologames engine are comprised of multiple layers to display itself with a holographic flair. Within the engine, layers work simialrly to how they would on graphic design software - some objects appear in front, while others remain in the back. Layers are integral to the hologames engine, and are perhaps even the main point.
---
title: "StateMachine"
---

Found in `component.py`.
Inherits from Behavior.

An implementation of the FSM (finite state machine) as a Behavior. Manages states by determining if there's a new state to enter to, or keep updating the current state otherwise.
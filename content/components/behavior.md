---
title: "Behavior"
---

Found in `component.py`.

Behaviors manipulate the GameObjects they're assigned to. This is similar to MonoBehaviours in Unity. Its lifecycle consists of `pre_apply`, `apply`, and `draw`. The pre-apply stage is where the behavior can reset its states and flags if necessary. The apply stage is where all the updating happens. The draw stage is where additional rendering and post-processing happens, drawn on top of its parent GameObject. It may be disabled temporarily by setting the `enabled` field to false.
---
title: "Client"
---

Found in `client.py`.

Represents the Hologames controller. Monitors the inputs given by a player through said controller and sends it to the server, which is how controls or commands are sent to the game. It is also capable of executing commands which entail playing sounds or sending vibrations through the controller.
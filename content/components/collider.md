---
title: "Collider"
---

Found in `component.py`.

Colliders are components that detect when a game object comes into contact with other game objects. In other words, it is the basis for hitboxes in games that are made with the Hologames engine.
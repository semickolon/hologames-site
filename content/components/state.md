---
title: "State"
---

Found in `component.py`.

States updates the behavior of a GameObject via the StateMachine class. e.g. a GameObject may be in either MoveState or IdleState depending on whether or not the player is sending any commands for movement.
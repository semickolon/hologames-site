---
title: "A. Introduction"
---

## What is "holographic"?
An illusory holographic effect is achieved when light reflects from a flat surface to a reflective surface, such as fiber glass, in a 45-degree angle perpendicular to the display. HoloGames makes use of this optical illusion to integrate depth into 2D games.

## How is this achieved?
There are several techniques as to how depth is achieved in such a manner. HoloGames supports any configuration you can think of by the use of **layers**. In a nutshell, layers are simply matrix transformations. Think of them as drawing boards where objects are drawn onto and eventually they are arranged in a way that depth is created. The default configuration uses two 1080x960 layers; one for the front and one for the back.

---
title: "Architecture"
---

## Wireless controllers
HoloGames makes use of wireless controllers. Currently, the only device platform supported is Android. Via UDP server-client communication, the server (engine) and client (player controller) can talk to each other. This means that the engine can support more than one client or player for multiplayer games.

## Core game loop
The main game holds a ScreenStack instance where the Screen instances are stored, pushed, and popped. The topmost item in the stack is the screen to be drawn. The Screen instance holds a list of GameObject instances, which in turn are updated and drawn as ordered by the game loop.

## Game objects
Inherits from Pyglet's Sprite class. GameObjects have a layer index property so that the parent Screen could determine if it should be rendered on the Layer it's currently on.  Going down further, each GameObject has a list of Behavior instances, which are also updated and optionally drawn.

## Behaviors
Following the Component programming pattern, the Behavior class are analogous to Unity's MonoBehaviour class. They modify their parent GameObject correspondingly.

## General lifecycle
1. Initialization
2. Update - mostly always supplied by delta time, the time from the last frame to the current frame.
3. Render/Draw
4. Disposal - to close or flush resources if necessary.

---
title: "Server & Client"
---

## Why UDP?
User Datagram Protocol was chosen due to its fast response times despite being unreliable as there's no guarantee that the other party received the message. Nonetheless, it is widely used for multiplayer games.

## Connection
Unlike TCP that requires connection as a prerequisite, UDP does not enforce this. However, the engine simulates this by enforcing the client to send a connection request first. Otherwise, the client's requests will be ignored. If accepted, the server will return a player number back to the client. Clients may disconnect by sending a disconnection request.

## Server
In the engine, the server holds a reference to its currently connected clients together with their corresponding player numbers or indices and IP addresses. The server is a Singleton and can be retrieved by `Server.get()`

## Client
In the engine, the Client class is made for handling and storing input events wherein other parts of the engine can respond and consume these events.

## Data exchange format
When both parties communicate, the message is a UTF-8 encoded string formatted like **COMMAND:PARAM** delimited by the initial colon after the command.

## Server commands
These are the commands that a client can send to the server.

**CONN:0**
Connect to the server. The parameter is ignored. If successful, the server returns the assigned player number.

**DCONN:0**
Disconnect from the server. The parameter is ignored.

**PRESS:XY**
Send controller key event.
*X* - key code
*Y* - up (0) or down (1) status

## Client commands
These are the commands that the server can send to a client.

**CONN:X**
Notify client about successful connection, where X is the client's assigned player number. This is zero-indexed so a number of 0 would mean that you are Player 1.

**ERR:X**
Send an error message where X is the message.

**VIBR:X**
Vibrate the client's device if possible where X is the duration in milliseconds. Useful for gameplay feedback.

**SOUND:X**
Play a sound clip in the client's device where X is the sound clip name.

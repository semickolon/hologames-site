# HoloGames

## A game engine designed for "holographic" displays accompanied by wireless controllers via UDP

## Features

* Customize holographic projection with layers
* Game objects, behaviors, animators, and more
* Server-client UDP communication
* Extensive life cycle